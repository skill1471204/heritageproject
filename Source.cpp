#include <iostream>


class Animal
{

public:
	virtual void voice()
	{
		std::cout << "animal sound";	
	}
	
};

class Wolf: public Animal
{

public:
	void voice() override
	{
		std::cout << "woof";
	}
	
};

class Sheep : public Animal
{

public:
	void voice() override
	{
		std::cout << "bleat";
	}

};

class Cat : public Animal
{
	
public:
	void voice() override
	{
		std::cout << "meow";
	}

};


template<class T>
T CreateGroup(T)
{


	T Group = new T[5];

	
	return Group;

}

int main()
{
	Cat Cat1;
	Cat Cat2;
	Wolf Wolf1;
	Sheep Sheep1;
	Wolf Wolf2;

	Animal* Animals[5] = { new Cat, new Wolf, new Sheep, new Animal };

	Animals[0] = &Cat1;
	Animals[1] = &Cat2;
	Animals[2] = &Wolf1;
	Animals[3] = &Sheep1;
	Animals[4] = &Wolf2;

	
	for (int i = 0; i < 5; i++)
	{
		Animal* p = Animals[i];
		p->voice();
		
		std::cout << "\n";
	}

}
	